const path = require('path');

module.exports = {
  webpack: {
    alias: {
      '@/components': path.resolve(__dirname, 'src/components'),
      '@/hooks': path.resolve(__dirname, 'src/hooks'),
      '@/images': path.resolve(__dirname, 'src/images'),
      '@/libs': path.resolve(__dirname, 'src/libs'),
      '@/models': path.resolve(__dirname, 'src/models'),
      '@/pages': path.resolve(__dirname, 'src/pages'),
      '@/routes': path.resolve(__dirname, 'src/routes'),
      '@/styles': path.resolve(__dirname, 'src/styles'),
      '@/utils': path.resolve(__dirname, 'src/utils'),
    },
  },
  style: {
    postcss: {
      mode: 'file',
    },
  },
  babel: {
    plugins: [
      [
        'reshadow/babel',
        {
          postcss: true,
        },
      ],
    ],
  },
};

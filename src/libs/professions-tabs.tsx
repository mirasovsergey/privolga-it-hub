import React, { FC, Children } from 'react';
import styled, { css } from 'reshadow';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

const styles = css`
  :global(.react-tabs__tab-list) {
    display: flex;
  }

  :global(.react-tabs__tab) {
    display: flex;
    align-items: center;
    height: 48px;
    padding: 0 24px;
    background: var(--white);
    border: 1px solid #cedce4;
    border-radius: 100px;
    font-weight: 500;
    font-size: 17px;
    line-height: 24px;
    outline: none;

    &:not(:last-of-type) {
      margin-right: 8px;
    }
  }

  :global(.react-tabs__tab--selected) {
    color: var(--white);
    background: #576b75;
  }

  p {
    padding: 42px 0 32px;
    letter-spacing: -0.02em;
    font-weight: 700;
    font-size: 40px;
    line-height: 56px;
  }
`;

type ProfessionsTabsProps = {
  className?: string;
  tabs: string[];
};

export const ProfessionsTabs: FC<ProfessionsTabsProps> = ({ className, tabs, children }) => {
  return styled(styles)(
    <Tabs className={className}>
      <TabList>
        <Tab>Все направления</Tab>
        {tabs.map(({ id, name }: any) => (
          <Tab key={`tab-${id}`}>{name}</Tab>
        ))}
      </TabList>

      <p>Профессии</p>

      {Children.map(children, (panel, index) => (
        <TabPanel key={`panel-${index}`}>{panel}</TabPanel>
      ))}
    </Tabs>,
  );
};

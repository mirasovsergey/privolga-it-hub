import React, { FC } from 'react';
import styled, { use, css } from 'reshadow';

const styles = css`
  div {
    position: relative;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;

    &::before {
      content: '';
      display: block;
      height: 2px;
      background-color: #7393a5;
      position: absolute;
    }

    &::after {
      content: '';
      display: block;
      height: 2px;
      background-color: #7393a5;
      transform: rotate(90deg);
      position: absolute;
    }

    &[use|role] {
      &=close {
        width: 48px;
        height: 48px;
        transform: rotate(45deg);
        border: 2px solid #7393a5;

        &::before,
        &::after {
          width: 22px;
        }
      }

      &=plus {
        width: 34px;
        height: 34px;
        border: 1px solid #7393a5;

        &::before,
        &::after {
          width: 15px;
        }
      }
    }
  }
`;

type IconProps = {
  className?: string;
  role: 'close' | 'plus';
};

export const Icon: FC<IconProps> = ({ className, role }) => {
  return styled(styles)``(<div className={className} {...use({ role })} />);
};

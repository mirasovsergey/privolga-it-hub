import { forward, createEffect, createEvent } from 'effector';

import { post } from '@/utils/api';

export const caseSelected = createEvent();

export const selectCaseFx = createEffect({
  handler: (data: any) =>
    post('/users/me/select-case/', {
      case: data.case,
    }),
});

forward({
  from: caseSelected,
  to: selectCaseFx,
});

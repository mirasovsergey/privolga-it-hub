import React, { FC, useEffect } from 'react';
import Modal from 'react-modal';
import styled, { css } from 'reshadow';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

import { Button } from '@/components/button';
import { Icon } from './components/icon';
import { caseSelected } from './model';

const Close = 'button';
const Label = 'p';
const Title = 'p';
const Duration = 'p';
const Image = 'img';
const Description = 'p';
const Cases = 'form';
const Case = 'label';
const Container = 'div';
const Content = 'div';
const CasesList = 'div';

const styles = css`
  :global(.ReactModal__Overlay) {
    background-color: rgb(5 13 26 / 0.3) !important;
    overflow: auto;
  }

  :global(.ReactModal__Content) {
    display: flex;
    flex-direction: column;
    background: var(--white);
    border-radius: 32px !important;
    width: 100%;
    max-width: 1008px;
    border: 0 !important;
    margin: 90px auto 90px;
    padding: 16px 16px 64px !important;
    position: relative !important;
    top: initial !important;
    right: initial !important;
    bottom: initial !important;
    left: initial !important;
  }

  Close {
    align-self: flex-end;
    margin-bottom: 8px;
  }

  Image {
    height: 359px;
    width: 227px;
    min-width: 227px;
    object-fit: cover;
    margin-right: 60px;
  }

  Container {
    padding: 0 100px;
  }

  Content {
    display: flex;
  }

  Label {
    background: #e5eefa;
    border-radius: 100px;
    display: inline-flex;
    align-items: center;
    padding: 0 14px;
    color: #17468f;
    font-weight: 500;
    font-size: 14px;
    height: 32px;
    margin-bottom: 8px;
  }

  Title {
    margin-bottom: 20px;
    letter-spacing: -0.02em;
    font-weight: 700;
    font-size: 52px;
    line-height: 56px;
  }

  Description {
    font-size: 17px;
    line-height: 28px;
    color: #28363e;
    margin-bottom: 24px;
  }

  Duration {
    font-size: 16px;
    line-height: 24px;

    & span {
      font-weight: 700;
    }
  }

  Cases {
    margin-top: 80px;
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;

    & p {
      letter-spacing: -0.02em;
      font-weight: 700;
      font-size: 40px;
      line-height: 48px;
      margin-bottom: 30px;
    }
  }

  Case {
    cursor: pointer;

    &:not(:last-of-type) {
      margin-right: 32px;
    }

    & div {
      border: 1px solid #c1cfd7;
      border-radius: 24px;
      width: 372px;
      height: 295px;
      display: flex;
      flex-direction: column;
      align-items: center;
    }

    & img {
      margin-bottom: 16px;
      max-height: 220px;
    }

    & p {
      letter-spacing: -0.02em;
      font-weight: 600;
      font-size: 24px;
      line-height: 32px;
    }

    & input {
      display: none;

      &:checked + div {
        box-shadow: 0 0 5px 1px #70a5f7;
      }
    }
  }

  Button {
    background: #ed5c23;
    border-radius: 16px;
    padding: 0 46px;
    height: 64px;
    display: flex;
    align-items: center;
    font-weight: 600;
    font-size: 24px;
    line-height: 24px;
    letter-spacing: -0.03em;
    color: var(--white);
    margin-top: 50px;
  }

  CasesList {
    display: flex;
  }
`;

type ProfessionModalProps = {
  className?: string;
  onToggle: () => void;
  isOpen: boolean;
  label?: string;
  title?: string;
  image?: string;
  cases?: any;
  description?: string;
  hours?: string;
};

Modal.setAppElement('#profession-modal');

export const ProfessionModal: FC<ProfessionModalProps> = ({
  className,
  onToggle,
  isOpen,
  image,
  hours,
  description,
  title,
  label,
  cases,
}) => {
  const { register, handleSubmit, formState } = useForm();
  const history = useHistory();
  const { isSubmitSuccessful } = formState;

  const onSubmit = (data: any) => {
    caseSelected(data);
  };

  useEffect(() => {
    if (isSubmitSuccessful) {
      history.push('/personal/cabinet');
    }
  }, [isSubmitSuccessful, history]);

  return styled(styles)(
    <Modal className={className} isOpen={isOpen} onRequestClose={onToggle}>
      <Close type="button" onClick={onToggle}>
        <Icon role="close" />
      </Close>

      <Container>
        <Content>
          <Image src={image} alt="" />

          <div>
            <Label>{label}</Label>
            <Title>Профессия {title}</Title>
            <Description>{description}</Description>
            <Duration>
              <span>Продолжительность</span>: {hours} часа
            </Duration>
          </div>
        </Content>

        <Cases onSubmit={handleSubmit(onSubmit)}>
          <p>Что будешь разрабатывать?</p>

          <CasesList>
            {cases.map(({ id, name, picture }: any) => (
              <Case key={id}>
                <input name="case" value={id} type="radio" ref={register} />
                <div>
                  <img src={picture} alt="" />
                  <p>{name}</p>
                </div>
              </Case>
            ))}
          </CasesList>

          <Button type="submit">Выбрать кейс</Button>
        </Cases>
      </Container>
    </Modal>,
  );
};

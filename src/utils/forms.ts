export const rules = {
  required: () => ({
    name: 'required',
    validator: (value: string) => Boolean(value),
  }),
};

export const errors = {
  required: 'Обязательное поле',
};

import React, { FC } from 'react';
import styled, { css } from 'reshadow';

import { Button as BaseButton } from '@/components/button';

const styles = css`
  BaseButton {
    width: 100%;
    height: 56px;
    background-color: var(--orange);
    color: var(--white);
    font-weight: 600;
    font-size: 20px;
    border-radius: 14px;
  }
`;

type ButtonProps = {
  onClick?: () => void;
  className?: string;
};

export const Button: FC<ButtonProps> = ({ onClick, children, className }) => {
  return styled(styles)(
    <BaseButton onClick={onClick} type="submit" className={className}>
      {children}
    </BaseButton>,
  );
};

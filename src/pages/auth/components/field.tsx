import React, { FC } from 'react';
import styled, { css } from 'reshadow';

import { errors } from '@/utils/forms';

const Container = 'div';
const Label = 'p';
const Error = 'p';

const styles = css`
  Container {
    width: 100%;
  }

  Label {
    margin-bottom: 4px;
    font-weight: 500;
    font-size: 18px;
    line-height: 24px;
    color: #7c95b6;
  }

  input {
    width: 100%;
    height: 56px;
    padding: 0 25px;
    background: #f8fafb;
    border: 1px solid #b1cbf1;
    border-radius: 14px;
    font-size: 18px;

    &::placeholder {
      font-weight: 500;
      font-size: 18px;
      color: #7c95b6;
    }
  }

  Error {
    margin-top: 4px;
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
    color: #ff5151;
  }
`;

type FieldProps = {
  label: string;
  type: string;
  fields: any;
  name: string;
  className?: string;
  placeholder?: string;
};

export const Field: FC<FieldProps> = ({ type, fields, name, label, className, placeholder }) => {
  const error = fields[name].errorText(errors);

  return styled(styles)(
    <Container className={className}>
      <label>
        <Label>{label}</Label>

        <input
          type={type}
          value={fields[name].value}
          onChange={({ target }) => fields[name].onChange(target.value)}
          placeholder={placeholder}
        />
      </label>

      {error ? <Error>{error}</Error> : null}
    </Container>,
  );
};

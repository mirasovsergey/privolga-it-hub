import React, { FC, FormEvent } from 'react';
import styled, { css } from 'reshadow';

const styles = css`
  form {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    max-width: 460px;
    background: var(--white);
    box-shadow: 0px 20px 50px rgba(74, 96, 130, 0.08);
    border-radius: 24px;
    padding: 36px 40px 56px;
  }
`;

type FormProps = {
  onSubmit: (e: FormEvent<EventTarget>) => void;
  className?: string;
};

export const Form: FC<FormProps> = ({ onSubmit, children, className }) => {
  return styled(styles)(
    <form onSubmit={onSubmit} className={className}>
      {children}
    </form>,
  );
};

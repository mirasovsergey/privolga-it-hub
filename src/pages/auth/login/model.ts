import { forward, createEffect } from 'effector';
import { createForm } from 'effector-forms';
import { $authUser } from '@/models/auth';

import { post } from '@/utils/api';
import { rules } from '@/utils/forms';

export const loginForm = createForm({
  fields: {
    login: {
      init: '',
      rules: [rules.required()],
    },
    password: {
      init: '',
      rules: [rules.required()],
    },
  },
  validateOn: ['submit'],
});

interface IFormHandler {
  login: string;
  password: string;
}

export const loginFx = createEffect({
  handler: ({ login, password }: IFormHandler) =>
    post('/users/auth/', {
      username: login,
      password,
    }),
});

forward({
  from: loginForm.formValidated,
  to: loginFx,
});

$authUser.on(loginFx.doneData, (_, user) => user);

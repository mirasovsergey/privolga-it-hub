import React, { FC, Fragment, FormEvent, useEffect } from 'react';
import styled, { css } from 'reshadow';
import { useForm } from 'effector-forms';
import { Link, useHistory } from 'react-router-dom';
import { useStore } from 'effector-react';

import { Form } from '@/pages/auth/components/form';
import { Field } from '@/pages/auth/components/field';
import { Button } from '@/pages/auth/components/button';
import { loginForm } from './model';
import { $isAuth } from '@/models/auth';

const ButtonLink = 'button';

const styles = css`
  Field {
    &:not(:last-of-type) {
      margin-bottom: 16px;
    }
  }

  Button {
    margin-top: 32px;
    margin-bottom: 24px;
  }

  Form {
    & > h1 {
      font-weight: 600;
      font-size: 48px;
      line-height: 48px;
      letter-spacing: -0.05em;
    }

    & > p {
      font-size: 16px;
      line-height: 24px;
      letter-spacing: -0.01em;
      margin-bottom: 36px;
    }
  }

  ButtonLink {
    margin-top: 40px;
  }
`;

export const Login: FC = () => {
  const { fields, submit } = useForm(loginForm);
  const isAuth = useStore($isAuth);
  const history = useHistory();

  useEffect(() => {
    if (isAuth) {
      history.push('/personal/home');
    }
  }, [history, isAuth]);

  const handleSubmit = (e: FormEvent<EventTarget>) => {
    e.preventDefault();
    submit();
  };

  return styled(styles)(
    <Fragment>
      <Form onSubmit={handleSubmit}>
        <h1>Вход</h1>
        <p>в портал кванториума</p>

        <Field type="text" name="login" label="Логин" fields={fields} />

        <Field type="password" name="password" label="Пароль" fields={fields} />

        <Button>Войти</Button>

        <Link to="/auth/restore" className="link">
          Забыли пароль?
        </Link>
      </Form>

      <ButtonLink className="link">Как получить логин и пароль?</ButtonLink>
    </Fragment>,
  );
};

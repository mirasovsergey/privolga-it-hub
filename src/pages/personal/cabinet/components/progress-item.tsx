import React, { FC } from 'react';
import styled, { css } from 'reshadow';

const Content = 'li';

const styles = css`
  Content {
    & :global(.progress-backdrop) {
      background: #fdeae3;
      border-radius: 12px;
      width: 100%;
      display: flex;
      padding: 2px;
      height: 16px;
    }

    & :global(.progress-line) {
      background: #ed5c23;
      border-radius: 6px;
      height: 100%;
    }

    & :global(.text) {
      font-weight: 500;
      font-size: 13px;
      line-height: 16px;
    }
  }
`;

type ProgressItemProps = {
  progress: number;
  text: string;
  className?: string;
};

export const ProgressItem: FC<ProgressItemProps> = ({ progress, text, className }) => {
  return styled(styles)(
    <Content className={className}>
      <div className="progress-backdrop">
        <div className="progress-line" style={{ width: `${progress}%` }} />
      </div>

      <p className="text">{text}</p>
    </Content>,
  );
};

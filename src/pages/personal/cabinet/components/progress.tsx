import React, { FC } from 'react';
import styled, { css } from 'reshadow';

import { ProgressItem } from './progress-item';

const Content = 'ul';

const styles = css`
  ProgressItem {
    &:not(:last-of-type) {
      margin-bottom: 12px;
    }
  }
`;

const progresses = [
  {
    progress: 80,
    text: 'Основы ООП',
  },
  {
    progress: 60,
    text: 'Наследование',
  },
  {
    progress: 50,
    text: 'Система типов',
  },
  {
    progress: 30,
    text: 'Ссылки и указатели',
  },
  {
    progress: 20,
    text: 'Рекурсия',
  },
];

export const Progress: FC = () => {
  return styled(styles)(
    <Content>
      {progresses.map(({ progress, text }: any, index) => (
        <ProgressItem key={index} progress={progress} text={text} />
      ))}
    </Content>,
  );
};

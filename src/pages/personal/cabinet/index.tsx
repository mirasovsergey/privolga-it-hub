import React, { FC } from 'react';
import styled, { css } from 'reshadow';
import { useStore } from 'effector-react';
import { useHistory } from 'react-router';

import { Header } from '../components/header';
import { Container } from '@/components/container';
import { Button } from '@/components/button';
import { Icon } from '@/components/icon';
import { Progress } from './components/progress';
import arrowRightIcon from '@/images/icons/arrow-right.svg';
import { $user } from '@/models/user';

const Content = 'div';
const Card = 'div';
const Cards = 'div';
const Icons = 'ul';

const styles = css`
  main {
    padding: 100px 0;
  }

  h1 {
    letter-spacing: -0.03em;
    font-weight: 700;
    font-size: 56px;
    line-height: 64px;
    margin-bottom: 50px;
  }

  Card {
    background: var(--white);
    box-shadow: 0px 20px 50px rgba(74, 96, 130, 0.1);
    border-radius: 24px;
    padding: 32px;
    transition: box-shadow 0.3s;

    &:hover {
      box-shadow: 0px 20px 50px rgba(74, 96, 130, 0.2);
    }

    &:global(.progress) {
      grid-column-start: 1;
      grid-column-end: 2;
      grid-row-start: 1;
      grid-row-end: 3;
      display: flex;
      flex-direction: column;
      align-items: center;
      cursor: pointer;

      & :global(.title) {
        letter-spacing: -0.02em;
        font-weight: bold;
        font-size: 24px;
        line-height: 24px;
        margin-top: 25px;
      }

      & :global(.image) {
        height: 267px;
        margin: 30px 0;

        & img {
          height: 100%;
          object-fit: cover;
        }
      }

      & :global(.label) {
        color: #17468f;
        font-weight: 500;
        font-size: 14px;
        line-height: 32px;
        padding: 0 14px;
        height: 32px;
        display: flex;
        align-items: center;
        background: #e5eefa;
        border-radius: 100px;
        margin-bottom: 14px;
      }

      & :global(.case) {
        letter-spacing: -0.02em;
        font-weight: 700;
        font-size: 40px;
        line-height: 48px;
        margin-bottom: 32px;
      }
    }

    &:global(.info) {
      grid-column-start: 2;
      grid-column-end: 4;
      grid-row-start: 1;
      grid-row-end: 2;
      display: flex;

      & :global(.image) {
        width: 96px;
        min-width: 96px;
        height: 96px;
        border-radius: 50%;
        background-color: #e0e5eb;
        margin-right: 24px;
      }

      & :global(.title) {
        font-weight: 700;
        font-size: 24px;
        line-height: 32px;
        margin-bottom: 12px;
      }

      & :global(.personal) {
        font-weight: 500;
        font-size: 15px;
        line-height: 24px;
        margin-bottom: 12px;

        & span {
          color: #7e7e7e;
        }
      }

      & Button {
        margin-top: 8px;
      }
    }

    &:global(.score) {
      & p {
        margin-bottom: 32px;
        font-weight: 700;
        font-size: 24px;
        line-height: 32px;
      }

      & div {
        background: #f2f4f8;
        border-radius: 16px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #5a708c;
        letter-spacing: -0.02em;
        font-weight: 700;
        font-size: 40px;
        line-height: 48px;
        height: 160px;
        margin-bottom: 32px;
      }
    }

    & Button {
      background: #fdeae3;
      border-radius: 12px;
      padding: 8px 16px;
      color: #d44811;
      letter-spacing: -0.01em;
      font-weight: 500;
      font-size: 15px;
      line-height: 24px;
    }

    &:global(.professions) {
      & :global(.title) {
        display: flex;
        align-items: center;
        justify-content: space-between;
        font-weight: 700;
        font-size: 24px;
        line-height: 32px;
      }

      & :global(.label) {
        font-weight: 500;
        font-size: 13px;
        line-height: 24px;
        margin-bottom: 16px;
      }
    }
  }

  Cards {
    display: grid;
    grid-template-columns: 480px 330px 330px;
    grid-template-rows: minmax(214px, 100%) minmax(334px, 100%);
    grid-gap: 29px;
  }

  Icons {
    display: flex;
    align-items: center;

    & li {
      width: 50px;
      height: 50px;
      background: #a9b0bc;
      border: 5px solid var(--white);
      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 50%;
      color: var(--white);
      font-weight: 700;
      margin: 0 -7px;
      z-index: 2;

      &:first-of-type {
        background: #ffffff;
        border: 3px solid #ed5c23;
        width: 40px;
        height: 40px;
        color: #ed5c23;
      }

      &:last-of-type {
        background: #e0e5eb;
        width: 40px;
        height: 40px;
        border: none;
        color: #000;
        letter-spacing: -0.01em;
        z-index: 1;
        margin-left: -2px;
      }
    }
  }
`;

export const Cabinet: FC = () => {
  const history = useHistory();

  const user = useStore<any>($user);

  const goToCase = () => {
    history.push('/personal/case/');
  };

  return styled(styles)(
    <Content>
      <Container maxWidth="1400px">
        <Header />
      </Container>

      <Container>
        <main>
          <h1>Личный кабинет</h1>

          <Cards>
            <Card className="progress" onClick={goToCase}>
              <p className="title">Текущий кейс</p>

              <div className="image">
                <img src={user?.selected_case?.case?.picture} alt="" />
              </div>

              <p className="label">{user?.selected_case?.case?.direction}</p>

              <p className="case">{user?.selected_case?.case?.name}</p>

              <Icons>
                <li>БГ</li>
                <li>БГ</li>
                <li>БГ</li>
                <li>БГ</li>
                <li>БГ</li>
                <li>БГ</li>
                <li>+15</li>
              </Icons>
            </Card>

            <Card className="info">
              <div className="image"></div>

              <div>
                <p className="title">
                  {user.first_name || ''} {user.last_name || ''}
                </p>
                <p className="personal">
                  <span>Email:</span> serj268@gmail.com
                </p>
                <p className="personal">
                  <span>Телефон:</span> +7 (999) 140-59-92
                </p>

                <Button type="button">Редактировать</Button>
              </div>
            </Card>

            <Card className="score">
              <p>Баллы</p>

              <div>1450</div>

              <Button type="button">Потратить</Button>
            </Card>

            <Card className="professions">
              <div className="title">
                Профессия <Icon src={arrowRightIcon} width="20px" />
              </div>

              <p className="label">Разработчик C++</p>

              <Progress />
            </Card>
          </Cards>
        </main>
      </Container>
    </Content>,
  );
};

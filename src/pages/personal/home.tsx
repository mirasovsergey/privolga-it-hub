import React, { FC, useEffect } from 'react';
import styled, { css } from 'reshadow';
import { useStore } from 'effector-react';

import { Header } from './components/header';
import { Container } from '@/components/container';
import { ProfessionsTabs } from '@/libs/professions-tabs';
import { ProfessionCard } from './components/profession-card';
import { $directions, getDirections } from '@/models/directions';
import { $professions, getProfessions } from '@/models/professions';

const Content = 'div';
const Grid = 'div';

const styles = css`
  main {
    padding: 100px 0;
  }

  h1 {
    font-weight: 700;
    font-size: 56px;
    line-height: 64px;
    letter-spacing: -0.03em;
    margin-bottom: 24px;
  }

  Grid {
    display: flex;
    flex-wrap: wrap;
  }

  ProfessionCard {
    margin-bottom: 30px;

    &:not(:nth-of-type(3n)) {
      margin-right: 29px;
    }
  }
`;

export const Home: FC = () => {
  const directions = useStore($directions);
  const professions = useStore($professions);

  useEffect(() => {
    getDirections();
    getProfessions();
  }, []);

  return styled(styles)(
    <Content>
      <Container maxWidth="1400px">
        <Header />
      </Container>

      <Container>
        <main>
          <h1>Выберите программу</h1>

          {directions.length && professions.length ? (
            <ProfessionsTabs tabs={directions}>
              <Grid>
                {professions.map(
                  ({ duration, picture, name, direction, desc, cases }: any, index: number) => (
                    <ProfessionCard
                      key={index}
                      label={directions.find(({ id }: any) => id === direction).name}
                      title={name}
                      hours={duration}
                      image={picture}
                      description={desc}
                      cases={cases}
                    />
                  ),
                )}
              </Grid>

              {directions.map(({ id, name: directioName }: any) => (
                <Grid key={id}>
                  {professions
                    .filter(({ direction }: any) => id === direction)
                    .map(({ duration, picture, name, cases, desc }: any, index: number) => (
                      <ProfessionCard
                        key={index}
                        label={directioName}
                        title={name}
                        hours={duration}
                        image={picture}
                        cases={cases}
                        description={desc}
                      />
                    ))}
                </Grid>
              ))}
            </ProfessionsTabs>
          ) : null}
        </main>
      </Container>
    </Content>,
  );
};

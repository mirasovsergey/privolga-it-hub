import React, { FC } from 'react';
import styled, { css } from 'reshadow';

import { Header } from './components/header';
import { Container } from '@/components/container';
import { Button } from '@/components/button';
import modelImage from '@/images/store/model.svg';
import antivirusImage from '@/images/store/antivirus.svg';
import bookImage from '@/images/store/book.svg';

const Content = 'div';
const Card = 'div';
const Items = 'div';

const styles = css`
  main {
    padding: 100px 0;
  }

  h1 {
    letter-spacing: -0.03em;
    font-weight: 700;
    font-size: 56px;
    line-height: 64px;
    margin-bottom: 60px;
  }

  Items {
    display: flex;
  }

  Card {
    width: 380px;
    background: #ffffff;
    box-shadow: 0px 20px 50px rgba(74, 96, 130, 0.1);
    border-radius: 24px;
    padding: 32px;
    display: flex;
    flex-direction: column;
    align-items: center;

    &:not(:nth-of-type(3n)) {
      margin-right: 29px;
    }

    & :global(.image) {
      height: 260px;
      max-height: 260px;
      width: 100%;
      margin-bottom: 20px;

      & img {
        height: 100%;
        width: 100%;
      }
    }

    & :global(.title) {
      text-align: center;
      letter-spacing: -0.02em;
      font-weight: 600;
      font-size: 18px;
      line-height: 24px;
      margin-bottom: 16px;
      margin-top: auto;
    }

    & :global(.price) {
      color: #ed5c23;
      font-weight: 600;
      font-size: 20px;
      line-height: 32px;
      margin-bottom: 16px;
    }
  }

  Button {
    background: #ed5c23;
    border-radius: 12px;
    padding: 8px 16px;
    letter-spacing: -0.01em;
    font-weight: bold;
    font-size: 15px;
    line-height: 24px;
    color: #ffffff;
  }
`;

export const Store: FC = () => {
  return styled(styles)(
    <Content>
      <Container maxWidth="1400px">
        <Header />
      </Container>

      <Container>
        <main>
          <h1>Магазин</h1>

          <Items>
            <Card>
              <div className="image">
                <img src={modelImage} alt="" />
              </div>

              <p className="title">Печать модели на 3D–принтере</p>
              <p className="price">700 баллов</p>

              <Button>Купить</Button>
            </Card>

            <Card>
              <div className="image">
                <img src={antivirusImage} alt="" />
              </div>

              <p className="title">Мастер класс по C++ от Лаборатории Касперского</p>
              <p className="price">500 баллов</p>

              <Button>Купить</Button>
            </Card>

            <Card>
              <div className="image">
                <img src={bookImage} alt="" />
              </div>

              <p className="title">Бонусные рубли сети «Читай город»</p>
              <p className="price">4 балла = 1 рубль</p>

              <Button>Купить</Button>
            </Card>
          </Items>
        </main>
      </Container>
    </Content>,
  );
};

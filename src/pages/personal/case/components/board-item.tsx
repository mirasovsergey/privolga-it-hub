import React, { FC } from 'react';
import styled, { css } from 'reshadow';

const Content = 'div';

const styles = css`
  Content {
    padding: 12px;
    background: #ffffff;
    box-shadow: 0px 10px 30px rgba(62, 81, 110, 0.12);
    border-radius: 8px;
  }
`;

type BoardItemProps = {
  text: string;
  className?: string;
};

export const BoardItem: FC<BoardItemProps> = React.forwardRef(
  ({ className, text, ...props }, ref: any) => {
    return styled(styles)(
      <Content className={className} {...props} ref={ref}>
        <p>{text}</p>
      </Content>,
    );
  },
);

import React, { FC, useState } from 'react';
import styled, { css } from 'reshadow';
import { DragDropContext, Draggable } from 'react-beautiful-dnd';
import { v4 as uuidv4 } from 'uuid';

import { BoardColumn } from './board-column';
import { BoardItem } from './board-item';

const ContentBoard = 'div';

const styles = css`
  ContentBoard {
    display: flex;

    & :global(.item) {
      &:not(:last-of-type) {
        margin-bottom: 10px;
      }
    }
  }

  BoardColumn {
    &:not(:last-of-type) {
      margin-right: 32px;
    }
  }
`;

const move = (source: any, destination: any, droppableSource: any, droppableDestination: any) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  // @ts-ignore
  result[droppableSource.droppableId] = sourceClone;
  // @ts-ignore
  result[droppableDestination.droppableId] = destClone;

  return result;
};

export const Board: FC = () => {
  const [tasks, setTasks] = useState({
    todo: [
      { title: 'Задача 1', id: `todo-${uuidv4()}` },
      { title: 'Задача 2', id: `todo-${uuidv4()}` },
    ],
    in_progress: [],
    done: [],
  });

  const handleDragEnd = (result: any) => {
    const { source, destination } = result;

    if (!destination) {
      return;
    }

    if (source.droppableId === destination.droppableId) {
    } else {
      const newTasks = move(
        // @ts-ignore
        tasks[source.droppableId],
        // @ts-ignore
        tasks[destination.droppableId],
        source,
        destination,
      );
      // @ts-ignore
      setTasks({
        ...tasks,
        ...newTasks,
      });
    }
  };

  const handleAddItem = (name: string): any => {
    // @ts-ignore
    const id = uuidv4();

    setTasks({
      ...tasks,
      // @ts-ignore
      [name]: [...tasks[name], { title: 'Новая задача', id: `${name}-${id}` }],
    });
  };

  return styled(styles)(
    <ContentBoard>
      <DragDropContext onDragEnd={handleDragEnd}>
        <BoardColumn
          title="К выполнению"
          droppableId="todo"
          onAddItem={() => handleAddItem('todo')}
        >
          {tasks['todo'].map((task, index) => (
            <Draggable key={task.id} draggableId={task.id} index={index}>
              {(provided: any, snapshot: any) => (
                <BoardItem
                  className="item"
                  text={task.title}
                  ref={provided.innerRef}
                  {...provided.draggableProps}
                  {...provided.dragHandleProps}
                  style={provided.draggableProps.style}
                />
              )}
            </Draggable>
          ))}
        </BoardColumn>

        <BoardColumn
          title="В процессе"
          droppableId="in_progress"
          onAddItem={() => handleAddItem('in_progress')}
        >
          {tasks['in_progress'].map((task, index) => (
            <Draggable
              // @ts-ignore
              key={task.id}
              // @ts-ignore
              draggableId={task.id}
              index={index}
            >
              {(provided: any, snapshot: any) => (
                <BoardItem
                  // @ts-ignore
                  text={task.title}
                  ref={provided.innerRef}
                  {...provided.draggableProps}
                  {...provided.dragHandleProps}
                  className="item"
                  style={provided.draggableProps.style}
                />
              )}
            </Draggable>
          ))}
        </BoardColumn>

        <BoardColumn title="Завершено" droppableId="done" onAddItem={() => handleAddItem('done')}>
          {tasks['done'].map((task, index) => (
            <Draggable
              // @ts-ignore
              key={task.id}
              // @ts-ignore
              draggableId={task.id}
              index={index}
            >
              {(provided: any, snapshot: any) => (
                <BoardItem
                  // @ts-ignore
                  text={task.title}
                  ref={provided.innerRef}
                  {...provided.draggableProps}
                  {...provided.dragHandleProps}
                  className="item"
                  style={provided.draggableProps.style}
                />
              )}
            </Draggable>
          ))}
        </BoardColumn>
      </DragDropContext>
    </ContentBoard>,
  );
};

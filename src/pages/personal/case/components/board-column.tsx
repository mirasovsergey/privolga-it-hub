import React, { FC } from 'react';
import styled, { css } from 'reshadow';
import { Droppable } from 'react-beautiful-dnd';

import { Button } from '@/components/button';

const Content = 'div';
const Header = 'div';
const Title = 'p';
const MenuButton = Button;
const AddButton = Button;
const Icon = 'div';

const styles = css`
  Content {
    max-width: 280px;
    width: 100%;

    & :global(.items) {
      padding: 16px 0 24px;
    }
  }

  MenuButton {
    display: flex;
    align-items: center;
    height: 32px;
    padding: 0;

    & span {
      display: block;
      width: 4px;
      height: 4px;
      background: #b1bccd;
      border-radius: 50%;

      &:not(:last-of-type) {
        margin-right: 2px;
      }
    }
  }

  AddButton {
    padding: 0 0 0 7px;
    display: flex;
    align-items: center;
    color: #7387a5;
    letter-spacing: -0.01em;
    font-weight: 500;
    font-size: 15px;
    line-height: 24px;
  }

  Icon {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 12px;

    &::before {
      content: '';
      display: block;
      width: 15px;
      height: 15px;
      height: 2px;
      background-color: #7387a5;
      position: absolute;
    }

    &::after {
      content: '';
      display: block;
      width: 15px;
      height: 15px;
      height: 2px;
      background-color: #7393a5;
      transform: rotate(90deg);
      position: absolute;
    }
  }

  Title {
    background: #dde7f8;
    border-radius: 8px;
    padding: 0 10px;
    height: 32px;
    display: inline-flex;
    align-items: center;
    color: #17468f;
    font-weight: 500;
    font-size: 15px;
    line-height: 24px;
  }

  Header {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;

type BoardColumnProps = {
  title: string;
  droppableId: string;
  className?: string;
  onAddItem?: () => void;
};

export const BoardColumn: FC<BoardColumnProps> = React.forwardRef(
  ({ children, title, onAddItem, className, droppableId }, ref: any) => {
    return styled(styles)(
      <Content className={className} ref={ref}>
        <Header>
          <Title>{title}</Title>
          <MenuButton>
            <span />
            <span />
            <span />
          </MenuButton>
        </Header>

        <Droppable droppableId={droppableId}>
          {(provided: any, snapshot: any) => (
            <div className="items" ref={provided.innerRef}>
              {children}
              {provided.placeholder}
            </div>
          )}
        </Droppable>

        <AddButton onClick={onAddItem}>
          <Icon />
          Добавить
        </AddButton>
      </Content>,
    );
  },
);

import React, { FC } from 'react';
import styled, { css } from 'reshadow';

const styles = css`
  ul {
    display: flex;
    align-items: center;
  }

  li {
    width: 50px;
    height: 50px;
    background: #a9b0bc;
    border: 5px solid var(--white);
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    color: var(--white);
    font-weight: 700;
    margin: 0 -7px;
    z-index: 2;

    &:first-of-type {
      background: #ffffff;
      border: 3px solid #ed5c23;
      width: 40px;
      height: 40px;
      color: #ed5c23;
    }

    &:last-of-type {
      background: #e0e5eb;
      width: 40px;
      height: 40px;
      border: none;
      color: #000;
      letter-spacing: -0.01em;
      z-index: 1;
      margin-left: -2px;
    }
  }
`;

export const UsersList: FC = () => {
  return styled(styles)(
    <ul>
      <li>БГ</li>
      <li>БГ</li>
      <li>БГ</li>
      <li>БГ</li>
      <li>БГ</li>
      <li>БГ</li>
      <li>+15</li>
    </ul>,
  );
};

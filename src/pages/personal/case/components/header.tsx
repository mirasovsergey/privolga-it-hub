import React, { FC } from 'react';
import styled, { css } from 'reshadow';

import { UsersList } from './users-list';
import { Icon } from '@/components/icon';
import { Button } from '@/components/button';
import arrowRightIcon from '@/images/icons/arrow-right.svg';
import gearIcon from '@/images/icons/gear.svg';
import chatIcon from '@/images/icons/chat.svg';

const Content = 'div';
const Info = 'div';
const ChatButton = 'button';

const styles = css`
  Content {
    margin-bottom: 60px;

    & :global(.title-wrapper) {
      display: flex;
      align-items: flex-end;

      & :global(.title) {
        margin-right: 17px;
        letter-spacing: -0.03em;
        font-weight: 700;
        font-size: 56px;
        line-height: 64px;
      }

      & Icon {
        margin-bottom: 10px;
      }
    }

    & :global(.left) {
      display: flex;
      align-items: center;
    }
  }

  Info {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  Icon {
    transform: rotate(180deg);
    margin-right: 9px;
    font-weight: 500;
    font-size: 15px;
    line-height: 32px;
  }

  Button {
    display: flex;
    align-items: center;
    color: #7387a5;
    padding: 0;
    margin-bottom: 16px;
  }

  ChatButton {
    position: relative;
    height: 40px;
    display: flex;
    align-items: center;
    padding: 0 19px;
    background: #e0e5eb;
    border-radius: 12px;
    font-weight: 500;
    font-size: 17px;
    line-height: 24px;
    letter-spacing: -0.01em;
    margin-left: 60px;

    & Icon {
      margin-right: 9px;
      transform: rotate(0deg);
    }

    & :global(.notify) {
      position: absolute;
      top: -9px;
      right: -7px;
      padding: 4px 7px;
      color: var(--white);
      font-weight: 700;
      font-size: 12px;
      line-height: 12px;
      letter-spacing: -0.01em;
      background: #ed5c23;
      border-radius: 12px;
    }
  }
`;

type HeaderProps = {
  className?: string;
  title: string;
};

export const Header: FC<HeaderProps> = ({ className, title }) => {
  return styled(styles)(
    <Content className={className}>
      <Button>
        <Icon src={arrowRightIcon} width="20px" /> Страница кейсов
      </Button>

      <Info>
        <div className="title-wrapper">
          <p className="title">{title}</p>

          <button type="button">
            <Icon src={gearIcon} width="24px" />
          </button>
        </div>

        <div className="left">
          <UsersList />

          <ChatButton>
            <Icon src={chatIcon} width="20px" /> Чат
            <span className="notify">20</span>
          </ChatButton>
        </div>
      </Info>
    </Content>,
  );
};

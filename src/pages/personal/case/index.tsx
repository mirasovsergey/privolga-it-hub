import React, { FC } from 'react';
import styled, { css } from 'reshadow';
import { useStore } from 'effector-react';

import { Header } from '../components/header';
import { Container } from '@/components/container';
import { Header as CaseHeader } from './components/header';
import { Board } from './components/board';
import { $user } from '@/models/user';

const Content = 'div';

const styles = css`
  main {
    padding: 100px 0;
  }
`;

export const Case: FC = () => {
  const user = useStore<any>($user);

  return styled(styles)(
    <Content>
      <Container maxWidth="1400px">
        <Header />
      </Container>

      <Container>
        <main>
          <CaseHeader title={user?.selected_case?.case?.name} />

          <Board />
        </main>
      </Container>
    </Content>,
  );
};

import React, { FC, useEffect } from 'react';
import styled, { css } from 'reshadow';
import { Link } from 'react-router-dom';
import { useStore } from 'effector-react';

import { Button } from '@/components/button';
import { Icon } from '@/components/icon';
import { Logo } from '@/components/logo';
import { Navigation } from './navigation';
import userIcon from '@/images/icons/user.svg';
import { loggedOut } from '@/models/auth';
import { $user, getUserInfo } from '@/models/user';

const styles = css`
  header {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 16px 0;

    & div {
      display: flex;
      align-items: center;

      & Navigation {
        margin-right: 80px;
      }
    }
  }

  Link {
    height: 48px;
    padding: 0 22px;
    display: inline-flex;
    align-items: center;
    background: var(--white);
    border: 1px solid #cedce4;
    border-radius: 100px;
    color: var(--black);
    font-weight: 500;
    font-size: 17px;
    line-height: 24px;

    & Icon {
      margin-right: 9px;
    }
  }

  Button {
    margin-left: 16px;
    letter-spacing: -0.01em;
    font-weight: 500;
    font-size: 17px;
    line-height: 24px;
  }
`;

export const Header: FC = () => {
  useEffect(() => {
    getUserInfo();
  }, []);

  const user = useStore<any>($user);

  return styled(styles)(
    <header>
      <Logo direction="row" width="40px" to="/personal/home" />

      <div>
        <Navigation />

        <Link to="/personal/cabinet">
          <Icon src={userIcon} width="19px" /> {user.first_name || ''} {user.last_name || ''}
        </Link>

        <Button type="button" onClick={loggedOut}>
          Выйти
        </Button>
      </div>
    </header>,
  );
};

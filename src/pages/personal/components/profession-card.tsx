import React, { FC, Fragment, useState } from 'react';
import styled, { css } from 'reshadow';

import { ProfessionModal } from '@/libs/profession-modal';

const Label = 'p';
const Title = 'p';
const Hours = 'p';

const styles = css`
  div {
    background: var(--white);
    box-shadow: 0px 20px 50px rgba(74, 96, 130, 0.1);
    border-radius: 24px;
    padding: 16px 16px 24px;
    max-width: 380px;
    min-height: 310px;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    transition: 0.25s box-shadow;
    background-size: 110px;
    background-repeat: no-repeat;
    background-position: center 45px;

    &:hover {
      cursor: pointer;
      box-shadow: 0px 20px 50px rgba(74, 96, 130, 0.2);
    }
  }

  Label {
    background: #e5eefa;
    border-radius: 100px;
    height: 32px;
    display: inline-flex;
    align-items: center;
    padding: 0 14px;
    font-weight: 500;
    font-size: 14px;
    line-height: 32px;
    color: var(--dark-blue);
    align-self: flex-start;
  }

  Title {
    font-weight: 700;
    font-size: 24px;
    line-height: 32px;
    letter-spacing: -0.02em;
    margin-top: auto;
  }

  Hours {
    font-size: 16px;
    line-height: 24px;
  }
`;

type ProfessionCardProps = {
  label: string;
  title: string;
  hours: string;
  className?: string;
  image?: string;
  cases?: any;
  descr?: string;
  description?: string;
};

export const ProfessionCard: FC<ProfessionCardProps> = ({
  label,
  className,
  title,
  hours,
  image,
  cases,
  description,
}) => {
  const [isOpen, setOpen] = useState(false);
  const toggle = () => setOpen(!isOpen);

  return styled(styles)(
    <Fragment>
      <div className={className} style={{ backgroundImage: `url(${image})` }} onClick={toggle}>
        <Label>{label}</Label>

        <Title>{title}</Title>
        <Hours>{hours} часа</Hours>
      </div>

      <ProfessionModal
        isOpen={isOpen}
        onToggle={toggle}
        label={label}
        title={title}
        image={image}
        cases={cases}
        hours={hours}
        description={description}
      />
    </Fragment>,
  );
};

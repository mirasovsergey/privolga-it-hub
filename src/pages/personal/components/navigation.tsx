import React, { FC } from 'react';
import styled, { css } from 'reshadow';
import { NavLink } from 'react-router-dom';

const styles = css`
  ul {
    display: flex;
    align-items: center;
  }

  li {
    letter-spacing: -0.01em;
    font-weight: 500;
    font-size: 17px;
    line-height: 24px;

    &:not(:last-of-type) {
      margin-right: 32px;
    }
  }

  NavLink {
    color: var(--black);
  }
`;

const list = [
  {
    text: 'Профессии',
    path: '/personal/home',
  },
  {
    text: 'Кейсы',
    path: '/personal/case',
  },
  {
    text: 'Магазин',
    path: '/personal/store',
  },
];

type NavigationProps = {
  className?: string;
};

export const Navigation: FC<NavigationProps> = ({ className }) => {
  return styled(styles)(
    <nav className={className}>
      <ul>
        {list.map(({ text, path }, index) => (
          <li key={index}>
            <NavLink to={path}>{text}</NavLink>
          </li>
        ))}
      </ul>
    </nav>,
  );
};

import React from 'react';
import { render } from 'react-dom';

import { App } from '@/components/app';
import './styles/libs.css';
import './styles/index.css';

const app = <App />;
const root = document.getElementById('root');

render(app, root);

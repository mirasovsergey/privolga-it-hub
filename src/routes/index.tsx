import React, { FC } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { useStore } from 'effector-react';

import { Guard } from '@/routes/components/guard';
import { Login } from '@/pages/auth/login';
import { Home } from '@/pages/personal/home';
import { Cabinet } from '@/pages/personal/cabinet';
import { Case } from '@/pages/personal/case';
import { Store } from '@/pages/personal/store';
import { Auth } from './components/auth';
import { $isAuth } from '@/models/auth';

export const Routes: FC = () => {
  const isAuth = useStore($isAuth);

  return (
    <Switch>
      <Guard
        path="/personal"
        isAuth={isAuth}
        routes={() => (
          <Switch>
            <Route path="/personal/home" exact component={Home} />
            <Route path="/personal/cabinet" exact component={Cabinet} />
            <Route path="/personal/case" exact component={Case} />
            <Route path="/personal/store" exact component={Store} />

            <Redirect to="/personal/home" />
          </Switch>
        )}
      />

      <Route path="/auth">
        <Auth>
          <Switch>
            <Route path="/auth/login" exact component={Login} />

            <Redirect to="/auth/login" />
          </Switch>
        </Auth>
      </Route>

      <Redirect to="/auth/login" />
    </Switch>
  );
};

import React, { FC } from 'react';
import styled, { css } from 'reshadow';

import { Logo } from '@/components/logo';

const styles = css`
  div {
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 100vh;
    padding: 35px 15px 35px;
    background-color: #f8fafb;
  }

  Logo {
    margin-bottom: 74px;
  }
`;

export const Auth: FC = ({ children }) => {
  return styled(styles)(
    <div>
      <Logo to="/auth/login" direction="column" width="60px" />

      {children}
    </div>,
  );
};

import React, { FC } from 'react';
import { Route, Redirect } from 'react-router-dom';

export const Guard: FC<any> = ({ routes: Routes, isAuth, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (isAuth ? <Routes {...props} /> : <Redirect to="/auth/login" />)}
  />
);

import { createStore, createEffect, forward, createEvent } from 'effector';

import { get } from '@/utils/api';

export const getProfessions = createEvent();
export const getProfessionsFx = createEffect({
  handler: () => get('/professions/'),
});
export const $professions = createStore<any>([]);

forward({
  from: getProfessions,
  to: getProfessionsFx,
});

$professions.on(getProfessionsFx.done, (_, data: any) => data.result);

import withStorage from 'effector-storage';
import { createEvent, createStore } from 'effector';

import { TOKEN_NAME } from '@/utils/api';

export const loggedOut = createEvent();

const createStorage = withStorage(createStore);

export const $authUser = createStore({
  token: null,
  role: '',
}).reset(loggedOut);

export const $token = createStorage(null, { key: TOKEN_NAME })
  .catch(console.error)
  .on($authUser, (_, user) => user.token)
  .reset(loggedOut);

export const $role = createStorage('', { key: 'Role' })
  .catch(console.error)
  .on($authUser, (_, user) => user.role)
  .reset(loggedOut);

export const $isAuth = $token.map(Boolean);

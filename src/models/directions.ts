import { createStore, createEffect, forward, createEvent } from 'effector';

import { get } from '@/utils/api';

export const getDirections = createEvent();
export const getDirectionsFx = createEffect({
  handler: () => get('/directions/'),
});
export const $directions = createStore<any>([]);

forward({
  from: getDirections,
  to: getDirectionsFx,
});

$directions.on(getDirectionsFx.done, (_, data: any) => data.result);

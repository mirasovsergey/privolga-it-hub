import { createEffect, createStore, createEvent, forward } from 'effector';

import { loggedOut } from './auth';
import { get } from '@/utils/api';

export const getUserInfo = createEvent();

export const $user = createStore({}).reset(loggedOut);

export const getUserInfoFx = createEffect({
  handler: () => get('/users/me/'),
});

forward({
  from: getUserInfo,
  to: getUserInfoFx,
});

$user.on(getUserInfoFx.doneData, (_, data: any) => data);

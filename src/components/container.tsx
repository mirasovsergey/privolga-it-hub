import React, { FC } from 'react';
import styled, { css } from 'reshadow';

const StyledContainer = 'div';
const StyledWrapper = 'div';

const styles = css`
  StyledContainer {
    width: 100%;
    padding: 0 8px;

    @media screen and (width >= 768px) {
      padding: 0 16px;
    }

    @media screen and (width >= 1200px) {
      padding: 0 32px;
    }
  }

  StyledWrapper {
    width: 100%;
    margin: 0 auto;
  }
`;

type ContainerProps = {
  containerClassName?: string;
  wrapperClassName?: string;

  maxWidth?: string;
};

export const Container: FC<ContainerProps> = ({
  children,
  containerClassName,
  wrapperClassName,
  maxWidth = '1200px',
}) => {
  return styled(styles)`
    StyledWrapper {
      max-width: ${maxWidth};
    }
  `(
    <StyledContainer className={containerClassName}>
      <StyledWrapper className={wrapperClassName}>{children}</StyledWrapper>
    </StyledContainer>,
  );
};

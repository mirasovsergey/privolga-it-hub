import React, { FC } from 'react';
import styled, { css } from 'reshadow';

const styles = css`
  button {
    padding: 0 15px;
  }
`;

type ButtonProps = {
  type?: 'button' | 'submit' | 'reset';
  onClick?: any;
  className?: string;
};

export const Button: FC<ButtonProps> = ({ children, type = 'button', onClick, className }) => {
  return styled(styles)(
    <button type={type} onClick={onClick} className={className}>
      {children}
    </button>,
  );
};

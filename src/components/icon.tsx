import React, { FC } from 'react';
import styled, { css } from 'reshadow';

const styles = css`
  img {
    width: 100%;
  }
`;

type IconProps = {
  src: string;
  alt?: string;
  className?: string;

  width?: string;
  height?: string;
  objectFit?: string;
};

export const Icon: FC<IconProps> = ({
  src,
  alt = '',
  width,
  height,
  objectFit = 'contain',
  className,
}) => {
  return styled(styles)`
    div {
      width: ${width};
      height: ${height};
    }

    img {
      object-fit: ${objectFit};
    }
  `(
    <div className={className}>
      <img src={src} alt={alt} />
    </div>,
  );
};

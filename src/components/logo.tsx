import React, { FC } from 'react';
import styled, { use, css } from 'reshadow';
import { Link } from 'react-router-dom';

import { Icon } from '@/components/icon';
import logoIcon from '@/images/logo.png';

const styles = css`
  Link {
    display: inline-flex;
    align-items: center;

    &[use|direction] {
      &=row {
        flex-direction: row;

        & p {
          margin-left: 10px;
        }
      }

      &=column {
        flex-direction: column;

        & p {
          margin-top: 10px;
        }
      }
    }
  }

  p {
    color: var(--dark-blue);
    text-transform: uppercase;
    font-weight: 600;
  }
`;

type LogoProps = {
  width?: string;
  direction: 'row' | 'column';
  className?: string;
  to: string;
};

export const Logo: FC<LogoProps> = ({ width = '50px', direction, className, to }) => {
  return styled(styles)(
    <Link to={to} {...use({ direction })} className={className}>
      <Icon src={logoIcon} alt="Кванториум" width={width} />

      <p>Кванториум</p>
    </Link>,
  );
};
